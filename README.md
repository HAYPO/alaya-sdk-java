# alaya-sdk-java

#### 介绍

提供一套简易使用的sdk，方便alaya节点和用户操作。将不定期新增功能。

#### 软件架构

基于maven构建的java sdk。

#### 安装教程

以下方法均可(建议使用ide)

> + 导入项目到IDEA，assembly后生成jar，然后运行`alaya-sdk-core-1.0-SNAPSHOT-jar-with-dependencies.jar`，将会执行example下的FastSendExample批量发送demo，操作如下：输入若干参数并回车：钱包密码，钱包路径，目的地址的csv文件路径和转账金额，之后会自动向目的地址发送金额。注意：csv文件中只需要第一列为地址即可，无需标题。
> + 导入项目到IDEA，在maven中选择install之后，在需要你的项目中添加如下依赖，即可在你的代码中调用sdk：
> ```xml
>  <dependency>
>    <groupId>com.cicdi</groupId>
>    <artifactId>alaya-sdk-core</artifactId>
>    <version>1.0-SNAPSHOT</version>
>  </dependency>
> ```
> + 直接复制源代码到你的项目，并导入相关依赖

#### `core`使用说明

+ 核心代码均在`core`里
+ API可以参考`resource`里的`alaya-sdk-core-全部文档.md`
+ `example`里有一些演示代码
+ `jar`里有打包好的jar，直接运行并按提示输入参数即可使用

#### `robot`使用说明
+ `robot`文件夹下有自动按照比例额外奖励的springboot程序（已实现发送奖励功能）
+ 导入ide，然后在ide内运行主程序。按提示输入密码和钱包地址。
+ 导入ide，然后package，之后在target文件夹下运行`alaya-robot-0.0.1-SNAPSHOT.jar`。按提示输入密码和钱包地址。
+ 将`application.yaml`文件与jar放在同一个文件夹下可以自定义参数，说明已在yaml文件中给出

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
