package com.cicdi.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

/**
 * 数学工具
 *
 * @author haypo
 * @date 2020/12/2
 */
public class MathUtil {
    /**
     * 比较两个big integer的大小，选择较大的
     *
     * @param a big integer
     * @param b big integer
     * @return 较大的big integer
     */
    public static BigInteger max(BigInteger a, BigInteger b) {
        return a.compareTo(b) > 0 ? a : b;
    }

    /**
     * 比较两个big decimal的大小，选择较大的
     *
     * @param a big decimal
     * @param b big decimal
     * @return 较大的big decimal
     */
    public static BigDecimal max(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) > 0 ? a : b;
    }

    /**
     * 比较两个int的大小，选择较小的
     *
     * @param a int
     * @param b int
     * @return 较小的int
     */
    public static int min(int a, int b) {
        return Math.min(a, b);
    }

    public static BigDecimal min(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) < 0 ? a : b;
    }

    public static BigInteger min(BigInteger a, BigInteger b) {
        return a.compareTo(b) < 0 ? a : b;
    }

    public static BigDecimal round2(BigDecimal b) {
        return round(b, 2);
    }

    public static BigDecimal round6(BigDecimal b) {
        return round(b, 6);
    }

    /**
     * 四舍五入，保留n位小数
     *
     * @param b 数值
     * @param n 小数位数
     * @return 四舍五入后的n位小数
     */
    public static BigDecimal round(BigDecimal b, int n) {
        return b.setScale(n, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal sum(List<BigDecimal> bigDecimalList) {
        BigDecimal sum = BigDecimal.ZERO;
        for (BigDecimal bigDecimal : bigDecimalList) {
            sum = sum.add(bigDecimal);
        }
        return sum;
    }

    /**
     * 开根号
     *
     * @param value 底数
     * @param scale 精度
     * @return 底数的平方根
     */
    public static BigDecimal sqrt(BigDecimal value, int scale) {
        BigDecimal num2 = BigDecimal.valueOf(2);
        int precision = 100;
        MathContext mc = new MathContext(precision, RoundingMode.HALF_UP);
        BigDecimal deviation = value;
        int cnt = 0;
        while (cnt < precision) {
            deviation = (deviation.add(value.divide(deviation, mc))).divide(num2, mc);
            cnt++;
        }
        deviation = deviation.setScale(scale, BigDecimal.ROUND_HALF_UP);
        return deviation;
    }
}
