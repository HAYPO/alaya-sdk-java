package com.cicdi.core.util;

import com.alaya.abi.solidity.datatypes.BytesType;
import com.alaya.abi.solidity.datatypes.generated.Uint16;
import com.alaya.abi.solidity.datatypes.generated.Uint256;
import com.alaya.abi.solidity.datatypes.generated.Uint64;
import com.alaya.bech32.Bech32;
import com.alaya.contracts.ppos.abi.CustomStaticArray;
import com.alaya.contracts.ppos.abi.Function;
import com.alaya.contracts.ppos.dto.enums.StakingAmountType;
import com.alaya.contracts.ppos.dto.resp.RestrictingPlan;
import com.alaya.utils.Numeric;
import com.cicdi.core.model.FunctionType;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

/**
 * 创建不同类型的function
 *
 * @author haypo
 * @date 2020/12/2
 */
public class FunctionUtil {
    /**
     * 创建一个委托的函数
     *
     * @param nodeId            节点id
     * @param stakingAmountType 自由还是锁仓
     * @param amount            金额
     * @return 函数
     */
    public static Function createDelegateFunction(String nodeId, StakingAmountType stakingAmountType, BigInteger amount) {
        return new Function(FunctionType.DELEGATE_FUNC_TYPE, Arrays.asList(new Uint16(stakingAmountType.getValue()), new BytesType(Numeric.hexStringToByteArray(nodeId)), new Uint256(amount)));
    }

    /**
     * 创建一个赎回委托的函数
     *
     * @param nodeId          节点id
     * @param stakingBlockNum 自由还是锁仓
     * @param amount          金额
     * @return 函数
     */
    public static Function createUnDelegateFunction(String nodeId, BigInteger stakingBlockNum, BigInteger amount) {
        return new Function(FunctionType.WITHDREW_DELEGATE_FUNC_TYPE, Arrays.asList(new Uint64(stakingBlockNum), new BytesType(Numeric.hexStringToByteArray(nodeId)), new Uint256(amount)));
    }

    public static Function createRestrictingPlanFunction(String account, List<RestrictingPlan> restrictingPlanList) {
        return new Function(
                com.alaya.contracts.ppos.dto.common.FunctionType.CREATE_RESTRICTINGPLAN_FUNC_TYPE,
                Arrays.asList(new BytesType(Bech32.addressDecode(account)), new CustomStaticArray<>(restrictingPlanList)));
    }
}
