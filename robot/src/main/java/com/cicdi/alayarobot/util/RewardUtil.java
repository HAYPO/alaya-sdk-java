package com.cicdi.alayarobot.util;

import com.alaya.contracts.ppos.RewardContract;
import com.alaya.contracts.ppos.dto.resp.Reward;
import com.alaya.parameters.NetworkParameters;
import com.cicdi.alayarobot.config.AlayaProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author haypo
 * @date 2020/12/7
 */
@Component
public class RewardUtil {
    @Autowired
    AlayaProperties alayaProperties;

    public List<Reward> getRewardList(String address, String nodeId) throws Exception {
        List<String> nodeIdList = new ArrayList<String>() {
            {
                add(nodeId);
            }
        };
        return RewardContract.load(alayaProperties.getWeb3j(), NetworkParameters.CurrentNetwork.getChainId())
                .getDelegateReward(address, nodeIdList).send().getData();
    }
}
