package com.cicdi.alayarobot.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author haypo
 * @date 2020/12/7
 */
@Slf4j
public class JsonUtil {
    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        //设置null属性值不参加序列无效
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        // 设置FAIL_ON_EMPTY_BEANS属性，当序列化空对象不要抛异常
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        // 设置FAIL_ON_UNKNOWN_PROPERTIES属性，当JSON字符串中存在Java对象没有的属性，忽略
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        // 设置null,忽略
        //   objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        //  objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, false);
    }

    /**
     * Convert JsonString to Simple Object
     *
     * @param jsonString
     * @param cls
     * @return
     */
    public static <T> T jsonString2SimpleObj(String jsonString, Class<T> cls) {
        T jsonObj = null;

        try {
            jsonObj = objectMapper.readValue(jsonString, cls);
        } catch (IOException e) {
            log.error("pasre json Object[{}] to string failed.", jsonString, e);
        }

        return jsonObj;
    }

    /**
     * Method that will convert from given value into instance of given value
     * type.
     *
     * @param fromValue
     * @param toValueType
     * @return
     * @throws
     */
    public static <T> T convertValue(Object fromValue, Class<T> toValueType)
            throws Exception {
        try {
            return objectMapper.convertValue(fromValue, toValueType);
        } catch (IllegalArgumentException e) {
            throw new Exception(e);
        }
    }

    public static String obj2Str(Object jsonObj) {
        String jsonString = null;

        try {
            jsonString = objectMapper.writeValueAsString(jsonObj);
        } catch (IOException e) {
            log.error("parse json Object[{}] to string failed.", jsonObj, e);
        }

        return jsonString;
    }

    /**
     * Method that will convert object to the ObjectNode.
     *
     * @param object the source data; if null, will return null.
     * @return the ObjectNode data after converted.
     * @throws
     */
    public static <T> ObjectNode convertObject2ObjectNode(T object)
            throws Exception {
        if (null == object) {
            return null;
        }

        ObjectNode objectNode = null;

        if (object instanceof String) {
            objectNode = convertJsonStringToObject((String) object,
                    ObjectNode.class);
        } else {
            objectNode = convertValue(object, ObjectNode.class);
        }

        return objectNode;
    }

    /**
     * Method that will convert the json string to destination by the type(cls).
     *
     * @param jsonString the source json string; if null, will return null.
     * @param cls        the destination data type.
     * @return
     * @throws
     */
    public static <T> T convertJsonStringToObject(String jsonString,
                                                  Class<T> cls) throws Exception {
        if (StringUtil.isBlank(jsonString)) {
            return null;
        }
        T object;
        try {
            object = objectMapper.readValue(jsonString, cls);
            return object;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);

        }
    }
}
