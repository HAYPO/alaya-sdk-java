package com.cicdi.alayarobot.util;

import java.util.Random;

/**
 * @author renhui
 * @date 2018/9/20 14:42
 */
public class StringUtil {
    static Random random = new Random();

    public static String underlineToHump(Object para) {
        return underlineToHump(String.valueOf(para));
    }



    public static String getRandomQQEmail(int length) {
        return getRandomString(length) + "@qq.com";
    }

    //length用户要求产生字符串的长度
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }



    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isUnderline(String str) {
        if (!isBlank(str)) {
            return str.matches("^\\w+_+\\w+$");
        } else {
            return false;
        }
    }

}
